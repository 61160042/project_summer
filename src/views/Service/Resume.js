// import Applicants from '../Service/Applicants'
const Resume = {
  resumeperson: [
    {
      id: 1,
      applicantId: 1,
      name: 'SHOW ID 1'
    },
    {
      id: 2,
      applicantId: 2,
      name: 'SHOW ID 2'
    }
  ],
  lastId: 3,
  getResume (id) {
    const index = this.resumeperson.findIndex(item => item.id === id)
    return { ...this.resumeperson[index] }
  }
}
export default Resume
