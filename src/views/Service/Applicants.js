const Applicants = {
  applicantList: [
    {
      id: 1,
      m_User: 'copterxi',
      m_Email: 'copterxi@hotmail.com',
      m_Password: 'copterxi1111',
      m_Firstname: 'Herican',
      m_Lastname: 'Pipat',
      m_Birthdate: '22/02/1985',
      m_Age: '36',
      m_Nationality: 'ไทย',
      m_Religion: 'พุทธ',
      m_Gender: 'ชาย',
      m_Weight: 69,
      m_Height: 169,
      m_Address: {
        m_Number: '43/2',
        m_Lane: '-',
        m_VillageNo: '5',
        m_District: 'บางบาล',
        m_SubDistrict: 'เสนา',
        m_Province: 'พระนครศรีอยุธยา',
        m_PostalCode: '13110'
      },
      m_Phonenumber: '0825488945'
    },
    {
      id: 2,
      m_User: 'Porche123',
      m_Email: 'Porche@hotmail.com',
      m_Password: 'Porche2222',
      m_Firstname: 'Porche',
      m_Lastname: 'Pause',
      m_Birthdate: '23/02/1985',
      m_Age: '36',
      m_Nationality: 'ไทย',
      m_Religion: 'พุทธ',
      m_Gender: 'ชาย',
      m_Weight: 79,
      m_Height: 177,
      m_Address: {
        m_Number: '45/5',
        m_Lane: '-',
        m_VillageNo: '5',
        m_District: 'อำเภอเมือง',
        m_SubDistrict: 'บางปลาสร้อย',
        m_Province: 'ชลบุรี',
        m_PostalCode: '20000'
      },
      m_Phonenumber: '0989888888'
    }
  ],
  lastId: 3,
  addApplicant (applicant) {
    applicant.id = this.lastId++
    this.applicantList.push(applicant)
  },
  getApplicants () {
    return [...this.applicantList]
  },
  getApplicant (id) {
    const index = this.applicantList.findIndex(item => item.id === id)
    return { ...this.applicantList[index] }
  },
  editHistory (applicantList) {
    const index = this.applicantList.findIndex(
      item => item.id === applicantList
    )
    this.applicantList.splice(index, 1, applicantList)
  }
}
export default Applicants
