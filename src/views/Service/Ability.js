const Ability = {
  Abilityperson: [
    {
      id: 1,
      resumeId: 1,
      aboutYourself: 'สามารถใช้โปรแกรม word,excel ได้คล่อง',
      skill: ['ภาษาอังกฤษ', 'ภาษาจีน'],
      wpm: ['ไทย 70 คำ/นาที', 'อังกฤษ 50 คำ/นาที']
    },
    {
      id: 2,
      resumeId: 2,
      aboutYourself: 'สามารถใช้โปรแกรม vi cs eclip ได้คล่อง',
      skill: ['ภาษาอังกฤษ', 'ภาษาเยอรมัน'],
      wpm: ['ไทย 80 คำ/นาที', 'อังกฤษ 60 คำ/นาที']
    }
  ],
  lastId: 3,
  getability (resumeId) {
    for (let index = 0; index < this.Abilityperson.length; index++) {
      if (resumeId === this.Abilityperson[index].resumeId) {
        return { ...this.Abilityperson[index] }
      } else {
        return {}
      }
    }
  }
}
export default Ability
