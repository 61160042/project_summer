const TrainingHistory = {
  Trainingperson: [
    {
      id: 1,
      resumeId: 1,
      timeTraining: 1,
      institution: 'อาคารสภาวิชาชีพบัญชี ถนนสุขุมวิท 21',
      courseTraining: 'การอบรมบัญชีดิจิทอล'
    },
    {
      id: 2,
      resumeId: 2,
      timeTraining: 2,
      institution: 'สถาบัน Data-sci',
      courseTraining: 'Basic Programing'
    }
  ],
  lastId: 3,
  gettraning (resumeId) {
    for (let index = 0; index < this.Trainingperson.length; index++) {
      if (resumeId === this.Trainingperson[index].resumeId) {
        return { ...this.Trainingperson[index] }
      } else {
        return {}
      }
    }
  }
}
export default TrainingHistory
