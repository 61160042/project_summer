const companyService = {
  currentCompany: [
    {
      id: 1,
      companyName: 'summer company',
      styleBusiness: 'การค้าเชิงพาณิชย์/ขนส่ง',
      security: 'ประกันสังคม,ค่าเบี้ยเลี้ยง',
      companyCode: '0000000000001',
      contactName: 'นพรดา กาเผือก',
      detailCompany: '',
      telCompany: '0945573069',
      addressCompany: {
        number: '179/2',
        lane: 'ซอยจันทร์ 31',
        villageNo: '5',
        district: 'สาทร',
        subDistrict: 'แขวงทุ่งวัดดอน',
        province: 'กรุงเทพ',
        postalCode: '10120'
      },
      website: 'summer.com',
      facebook: 'summer_company',
      statusCheckCompany: 'passed',
      username: 'summer_company',
      password: 'pp61160145-27',
      email: 'Summer@gmail.com',
      status: 'member'
    },
    {
      id: 2,
      companyName: 'summer company222',
      styleBusiness: 'การค้าเชิงพาณิชย์/ขนส่ง',
      security: 'ประกันสังคม,ค่าเบี้ยเลี้ยง',
      companyCode: '0000000000001',
      contactName: 'นพรดา กาเผือก',
      detailCompany: '',
      telCompany: '0945573069',
      addressCompany: {
        number: '179/2',
        lane: 'ซอยจันทร์ 31',
        villageNo: '5',
        district: 'สาทร',
        subDistrict: 'แขวงทุ่งวัดดอน',
        province: 'ชลบุรี',
        postalCode: '10120'
      },
      website: 'summer.com',
      facebook: 'summer_company',
      statusCheckCompany: 'passed',
      username: 'summer_company',
      password: 'pp61160145-27',
      email: 'Summer@gmail.com',
      status: 'member'
    },
    {
      id: 3,
      companyName: 'summer333',
      styleBusiness: 'การค้าเชิงพาณิชย์/ขนส่ง',
      security: 'ประกันสังคม,ค่าเบี้ยเลี้ยง',
      companyCode: '0000000000001',
      contactName: 'นพรดา กาเผือก',
      detailCompany: '',
      telCompany: '0945573069',
      addressCompany: {
        number: '179/2',
        lane: 'ซอยจันทร์ 31',
        villageNo: '5',
        district: 'สาทร',
        subDistrict: 'แขวงทุ่งวัดดอน',
        province: 'จันทบุรี',
        postalCode: '10120'
      },
      website: 'summer.com',
      facebook: 'summer_company',
      statusCheckCompany: 'passed',
      username: 'summer_company',
      password: 'pp61160145-27',
      email: 'Summer@gmail.com',
      status: 'member'
    },
    {
      id: 4,
      companyName: 'AAAAAA',
      styleBusiness: 'การค้าเชิงพาณิชย์/ขนส่ง',
      security: 'ประกันสังคม,ค่าเบี้ยเลี้ยง',
      companyCode: '0000000000001',
      contactName: 'นพรดา กาเผือก',
      detailCompany: '',
      telCompany: '0945573069',
      addressCompany: {
        number: '179/2',
        lane: 'ซอยจันทร์ 31',
        villageNo: '5',
        district: 'สาทร',
        subDistrict: 'แขวงทุ่งวัดดอน',
        province: 'จันทบุรี',
        postalCode: '10120'
      },
      website: 'summer.com',
      facebook: 'summer_company',
      statusCheckCompany: 'passed',
      username: 'summer_company',
      password: 'pp61160145-27',
      email: 'Summer@gmail.com',
      status: 'member'
    },
    {
      id: 5,
      companyName: 'BBBBB',
      styleBusiness: 'การค้าเชิงพาณิชย์/ขนส่ง',
      security: 'ประกันสังคม,ค่าเบี้ยเลี้ยง',
      companyCode: '0000000000001',
      contactName: 'นพรดา กาเผือก',
      detailCompany: '',
      telCompany: '0945573069',
      addressCompany: {
        number: '179/2',
        lane: 'ซอยจันทร์ 31',
        villageNo: '5',
        district: 'สาทร',
        subDistrict: 'แขวงทุ่งวัดดอน',
        province: 'นนทบุรี',
        postalCode: '10120'
      },
      website: 'summer.com',
      facebook: 'summer_company',
      statusCheckCompany: 'passed',
      username: 'summer_company',
      password: 'pp61160145-27',
      email: 'Summer@gmail.com',
      status: 'member'
    }
  ],
  lastId: 6,
  getcompany (id) {
    const index = this.currentCompany.findIndex(item => item.id === id)
    return { ...this.currentCompany[index] }
  },
  getcompanies () {
    return [...this.currentCompany]
  },
  addCompany (company) {
    company.id = this.lastId++
    this.currentCompany.push(company)
  },
  updateCompany (company) {
    console.log('update Company jaaaa')
    console.log(company)
    const index = this.currentCompany.findIndex(item => item.id === company.id)
    this.currentCompany.splice(index, 1, company)
  }
}
export default companyService
