const Application = {
  application: [
    {
      No: 1,
      position: 'พนักงานบัญชี',
      countApplication: '2'
    },
    {
      No: 2,
      position: 'เจ้าหน้าที่การคลัง',
      countApplication: '2'
    }
  ],
  lastId: 3,
  getApplications () {
    return [...this.application]
  }
}
export default Application
