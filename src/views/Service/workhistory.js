const workhistoryservice = {
  workhistoryperson: [
    {
      workhistoryId: 1,
      resumeId: 1,
      workExerience: 1,
      nameCompany: 'summer Company',
      addressCompany: '79/2 ซอยจันทร์ 31 5 สาทร แขวงทุ่งวัดดอน กรุงเทพ 10120',
      salary: 17000.0,
      position: 'พนักงานบัญชี',
      responsibiliti: 'ดูการเงินและบัญชีภายในบริษัท',
      time: '3 ปี'
    },
    {
      workhistoryId: 2,
      resumeId: 2,
      workExerience: 1,
      nameCompany: 'summer Company',
      addressCompany: '99/109 ซอยศรีลม เมือง แขวงบ้านใต้ กาญจนบรุี 71000',
      salary: 10000.0,
      position: 'พนักงานเสิร์ฟ',
      responsibiliti: 'ดูแลบริการการจัดเตรียมเครื่องอาหารให้กับลูกค้า',
      time: '3 ปี'
    }
  ],
  lastId: 3,
  getWorkhis (resumeId) {
    for (let index = 0; index < this.workhistoryperson.length; index++) {
      if (resumeId === this.workhistoryperson[index].resumeId) {
        return { ...this.workhistoryperson[index] }
      } else {
        return {}
      }
    }
  },
  addWorkhist (work) {
    work.id = this.lastId++
    this.workhistoryperson.push(work)
  }
}
export default workhistoryservice
