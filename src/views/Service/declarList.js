import companyService from '../Service/company'
const Declare = {
  // declareList: [],
  declareList: [
    {
      _id: '603bdacc70656b3420cacd8f',
      id: 1,
      companyId: 1,
      dateStart: '2021-02-14',
      dateClose: '2021-02-28',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี1',
        workType: 'บัญชี-ธนาคาร',
        gender: 'F',
        age: '30-35',
        rate: 2,
        salary: 17000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' },
          { detail: 'มีความรับผิดชอบ ขยัน อดทน ซื่อสัตย์ ตรงต่อเวลา' },
          { detail: 'มีความกระตือรือร้น และทำงานภายใต้ความกดดันได้ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 2,
      companyId: 1,
      dateStart: '2021-02-28',
      dateClose: '2021-03-102',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี',
        workType: 'บัญชี-ธนาคาร',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 3,
      companyId: 1,
      dateStart: '2021-02-28',
      dateClose: '2021-03-10',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี3',
        workType: 'บัญชี-ธนาคาร',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 4,
      companyId: 2,
      dateStart: '2021-02-28',
      dateClose: '2021-03-10',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี4',
        workType: 'บัญชี-ธนาคาร',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 5,
      companyId: 2,
      dateStart: '2021-02-28',
      dateClose: '2021-03-10',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี5',
        workType: 'บัญชี-ธนาคาร',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 6,
      companyId: 1,
      dateStart: '2021-02-28',
      dateClose: '2021-03-10',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี6',
        workType: 'บัญชี-ธนาคาร',
        gender: 'M',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 7,
      companyId: 1,
      dateStart: '2021-03-01',
      dateClose: '2021-03-10',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี7',
        workType: 'บัญชี-ธนาคาร',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 8,
      companyId: 1,
      dateStart: '2021-03-01',
      dateClose: '2021-03-15',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานบัญชี8',
        workType: 'บัญชี-ธนาคาร',
        gender: 'F',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 9,
      companyId: 3,
      dateStart: '2021-03-01',
      dateClose: '2021-03-15',
      statusDeclare: 'success',
      position: {
        positionName: 'พนักงานการเงิน',
        workType: 'บัญชี-ธนาคาร',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 10,
      companyId: 4,
      dateStart: '2021-03-01',
      dateClose: '2021-03-15',
      statusDeclare: 'success',
      position: {
        positionName: 'CEO',
        workType: 'การเงิน',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    },
    {
      id: 11,
      companyId: 5,
      dateStart: '2021-03-01',
      dateClose: '2021-03-15',
      statusDeclare: 'success',
      position: {
        positionName: 'TTTTTTTTT',
        workType: 'การเงิน',
        gender: 'A',
        age: '30-50',
        rate: 10,
        salary: 15000,
        detail: 'รายงานภาษีซื้อขายยื่นต่อกรมสรรพากรตามกำหนดทุกเดือน',
        property: [
          { detail: 'วุฒิ ปวช. ขึ้นไป' },
          { detail: 'มีความรับผิดชอบสูง ทัศนคติที่ดี' }
        ],
        note: null
      },
      countApplication: '2'
    }
  ],
  declares: [],
  lastId: 12,
  lastList: [],
  companies: [],
  currentDeclares: [],
  id: 0,
  getDeclareList (currentCompany) {
    this.declares = []
    for (let index = 0; index < this.declareList.length; index++) {
      if (this.declareList[index].companyId === currentCompany.id) {
        this.declares.push(this.declareList[index])
      }
    }
    return [...this.declares]
  },
  getLastDeclares (currentCompany) {
    this.declares = []
    this.lastList = []
    this.declares = this.getDeclareList(currentCompany).reverse()
    if (this.declares.length < 4) {
      this.lastList = this.declares
      return [...this.lastList]
    }
    for (let index = 0; index < 4; index++) {
      if (this.declares[index].companyId === currentCompany.id) {
        this.lastList.push(this.declares[index])
      }
    }
    return [...this.lastList]
  },
  getDeclare (currentCompany, id) {
    this.declares = []
    this.declares = this.getDeclareList(currentCompany)
    for (let index = 0; index < this.declares.length; index++) {
      if (this.declares[index].id === id) {
        return { ...this.declares[index] }
      }
    }
  },
  getCountDeclares () {
    return this.declareList.length
  },
  getDeclares () {
    return [...this.declareList]
  },
  getDeclaresBangkok () {
    this.declares = []
    this.companies = []
    this.companies = companyService.getcompanies()
    for (let index = 0; index < this.companies.length; index++) {
      if (this.companies[index].addressCompany.province === 'กรุงเทพ') {
        this.declares = this.getDeclareList(this.companies[index])
      }
    }
    return [...this.declares]
  },
  getDeclaresEast () {
    this.currentDeclares = []
    this.declares = []
    this.companies = []
    this.companies = companyService.getcompanies()
    for (let index = 0; index < this.companies.length; index++) {
      if (this.companies[index].addressCompany.province === 'ชลบุรี') {
        this.declares = this.getDeclareList(this.companies[index])

        // for (let i = 0; i < this.currentDeclares.length; i++) {
        //   this.declares.push(this.currentDeclares[i])
        // }
      }
    }
    return [...this.declares]
    // return [...this.currentDeclares]
  },
  getDeclaresNorth () {
    this.declares = []
    this.companies = []
    this.companies = companyService.getcompanies()
    for (let index = 0; index < this.companies.length; index++) {
      if (this.companies[index].addressCompany.province === 'เชียงใหม่') {
        this.declares = this.getDeclareList(this.companies[index])
      }
    }
    return [...this.declares]
  },
  getDeclaresNortheast () {
    this.declares = []
    this.companies = []
    this.companies = companyService.getcompanies()
    for (let index = 0; index < this.companies.length; index++) {
      if (this.companies[index].addressCompany.province === 'ขอนแก่น') {
        this.declares = this.getDeclareList(this.companies[index])
      }
    }
    return [...this.declares]
  },
  getDeclaresCentralRegion () {
    this.declares = []
    this.companies = []
    this.companies = companyService.getcompanies()
    for (let index = 0; index < this.companies.length; index++) {
      if (this.companies[index].addressCompany.province === 'นนทบุรี') {
        this.declares = this.getDeclareList(this.companies[index])
      }
    }
    return [...this.declares]
  },
  getDeclaresSouth () {
    this.declares = []
    this.companies = []
    this.companies = companyService.getcompanies()
    for (let index = 0; index < this.companies.length; index++) {
      if (this.companies[index].addressCompany.province === 'ภูเก็ต') {
        this.declares = this.getDeclareList(this.companies[index])
      }
    }
    return [...this.declares]
  },
  getSearching (workType, province, district, findWord) {
    this.declares = []
    this.currentDeclares = []
    this.companies = []
    this.companies = companyService.getcompanies()
    if (
      workType === null &&
      province === null &&
      district === null &&
      findWord === ''
    ) {
      return this.getDeclares()
    } else if (
      workType !== null &&
      province === null &&
      district === null &&
      findWord === ''
    ) {
      for (let index = 0; index < this.declareList.length; index++) {
        if (this.declareList[index].position.workType === workType) {
          this.declares.push(this.declareList[index])
        }
      }
    } else if (
      workType === null &&
      province !== null &&
      district === null &&
      findWord === ''
    ) {
      for (let index = 0; index < this.companies.length; index++) {
        if (this.companies[index].addressCompany.province === province) {
          this.declares = this.getDeclareList(this.companies[index])
        }
      }
    } else if (
      workType === null &&
      province === null &&
      district === null &&
      findWord !== ''
    ) {
      for (let index = 0; index < this.declareList.length; index++) {
        if (
          this.declareList[index].position.positionName.search(findWord) > 0
        ) {
          this.declares.push(this.declareList[index])
        }
      }
    }
    return [...this.declares]
  },
  addDeclare (declare) {
    declare.id = this.lastId++
    this.declareList.push(declare)
  }
}
export default Declare
