import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomeCompany',
    component: () => import('../views/Company/Home.vue')
  },
  {
    path: '/declare',
    name: 'declare',
    component: () => import('../views/Company/Declare.vue')
  },
  {
    path: '/detailDeclare/:id',
    name: 'DetailDeclare',
    component: () => import('../views/Company/DetailDeclare.vue'),
    props: true
  },
  {
    path: '/detailDeclareApplicant/:id',
    name: 'DetailDeclareApplicant',
    component: () => import('../views/DetailDeclareApplicant.vue'),
    props: true
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/addnotice',
    name: 'Addnotice',
    component: () => import('../views/Company/Addnotice.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/Login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/editProfile',
    name: 'EditProfile',
    component: () => import('../views/Company/EditProfile.vue')
  },
  {
    path: '/jobSearch',
    name: 'JobSearch',
    component: () => import('../views/Applicant/JobSearch.vue')
  },
  {
    path: '/History',
    name: 'history',
    component: () => import('../views/History.vue')
  },
  {
    path: '/registerApplicant',
    name: 'registerApplicant',
    component: () => import('../views/Applicant/ApplicantRegister.vue')
  },
  {
    path: '/Contact',
    name: 'contact',
    component: () => import('../views/Contact.vue')
  },
  {
    path: '/application',
    name: 'Application',
    component: () => import('../views/Company/application.vue')
  },
  {
    path: '/homeapplicant',
    name: 'Homeapplicant',
    component: () => import('../views/Applicant/Homeapplicant.vue')
  },
  {
    path: '/selectApplicant',
    name: 'SelectApplicant',
    component: () => import('../views/Company/SelectApplicant.vue')
  },
  {
    path: '/resume',
    name: 'Resume',
    component: () => import('../views/Company/Resume.vue')
  },
  {
    path: '/results',
    name: 'Results',
    component: () => import('../views/Applicant/Results.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
